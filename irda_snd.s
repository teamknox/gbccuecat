;*************************************************
;*                                               *
;*         IR DRIVER for  Game Boy Color         *
;*           [ IrDA SIR 1.0 Sender ]             *
;*                                               *
;*                   2000/09                     *
;*               K.I. @ TeamKNOx                 *
;* --------------------------------------------- *
;*   - set -                                     *
;*         speed   ;0:2400bps, 1:9600bps         *
;*         parity  ;0:non, 1:odd, 2:even         *
;*   - input -                                   *
;*         send data(1 byte)                     *
;*   - output -                                  *
;*         none                                  *
;*************************************************
;clock calc.
;  4.194304MHz /  2400bps = 1747.6clock
;  4.194304MHz /  9600bps =  436.9clock
;pulse width
;   1.6us / 4.194304MHz = 6.7clock

        .globl _speed           ;0:2400bps, 1:9600bps
        .globl _parity          ;0:non, 1:odd, 2:even

        .area   _CODE
_irda_snd::
        di
        xor     a
        ldh     (#0x56),a       ;read disable

;*** Send Data ***
        lda     hl,2(sp)        ;skip return address
        ld      a,(hl)          ;send data
        cpl
        ld      d,a

        ld      a,(#_speed)
        ld      h,a
        ld      a,(#_parity)
        ld      l,a

        ld      a,l
        cp      #<0
        jr      z,snd_start

        ld      c,#<8           ;bit counter
        ld      e,#<0           ;parity counter
1$:     ld      a,d
        rrca
        ld      d,a
        xor     e
        ld      e,a
        dec     c
        jr      nz,1$

        xor     l
        ld      e,a             ;parity set

;*** Start Bit ***
snd_start:
        ld      a,#0x01
        ldh     (#0x56),a
        xor     a                                       ; 4
        ldh     (#0x56),a                               ;12 
        call    delay                                   ;24+delay
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4

;*** Data Bit ***
        ld      c,#<8           ;bit counter            ; 8
snd_data:
        ld      a,d                                     ; 4
        and     #0x01                                   ; 8
        ldh     (#0x56),a                               ;12
                                        ;===== total:   108+delay =====
        xor     a                                       ; 4
        ldh     (#0x56),a                               ;12
        ld      a,d                                     ; 4
        rrca                                            ; 4
        ld      d,a                                     ; 4
        call    delay                                   ;24+delay
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        dec     c                                       ; 4
        jr      nz,snd_data                             ;12(8)
                                        ;===== total:   108+delay =====
;*** Parity Bit ***
        ld      a,l                                     ; 4
        cp      #<0                                     ; 8
        jr      z,snd_stop                              ; 8(12)

        ld      a,e                                     ; 4
        and     #0x01                                   ; 8
        ldh     (#0x56),a                               ;12
                                        ;===== total:   108+delay =====
        xor     a                                       ; 4
        ldh     (#0x56),a                               ;12
        call    delay                                   ;24+delay
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4
        nop                                             ; 4

;*** Stop Bit ***
snd_stop:
        xor     a                                       ; 4
        ldh     (#0x56),a                               ;12
                                        ;===== total:   108+delay =====
        ei
        ret

;=================================================
; delay
;       input           :h
;       output          :non
;       destroyed       :a, b
;=================================================
delay:
        ld      a,h                     ; 4
        rrca                            ; 4
        jr      c,d96                   ;12(8)
        jr      d24                     ;12

d96:
        ld      b,#<18                  ; 8
1$:
        dec     b                       ; 4
        jr      nz,1$                   ;12(8)

        ret                             ;16
                        ;===== total:    16*18+40=328 =====

d24:
        ld      b,#<99                  ; 8
1$:
        dec     b                       ; 4
        jr      nz,1$                   ;12(8)

        nop                             ; 4
        nop                             ; 4
        ret                             ;16
                        ;===== total:    16*99+56=1640 =====

;end of irda_snd.s
