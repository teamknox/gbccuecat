# Cuecat reader handling by Gameboy color
Basic Driver for :CueCAT which has been free-distributed in United States. 

<table>
<tr>
<td><img src="./pics/gb-cat_use.jpg"></td>
</tr>
</table>

## How it works
Electric Charactristic is same as GB-KEY. Output of CueCAT is encoded by Base64. 

## Examples
Connection and reading.

<table>
<tr>
<td><img src="./pics/gb-cat_sys.jpg"></td>
</tr>
</table>


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 

