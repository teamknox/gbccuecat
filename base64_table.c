//
// Base64 encode table
//
UBYTE base64_table[] = { 
    0xFF,   //              00
    0xFF,   //              01
    0xFF,   //              02
    0xFF,   //              03
    0xFF,   //              04
    0xFF,   //              05
    0xFF,   //              06
    0xFF,   //              07
    0xFF,   //              08
    0xFF,   //              09
    0xFF,   //              0A
    0xFF,   //              0B
    0xFF,   //              0C
    0xFF,   //              0D
    0xFF,   //              0E
    0xFF,   //              0F
    0xFF,   //              10
    0xFF,   //              11
    0xFF,   //              12
    0xFF,   //              13
    0xFF,   //              14
    0xFF,   //              15
    0xFF,   //              16
    0xFF,   //              17
    0xFF,   //              18
    0xFF,   //              19
    0xFF,   //              1A
    0xFF,   //              1B
    0xFF,   //              1C
    0xFF,   //              1D
    0xFF,   //              1E
    0xFF,   //              1F
    0xFF,   //              20
    0xFF,   //              21
    0xFF,   //              22
    0xFF,   //              23
    0xFF,   //              24
    0xFF,   //              25
    0xFF,   //              26
    0xFF,   //              27
    0xFF,   //              28
    0xFF,   //              29
    0xFF,   //              2A
    0xFF,   //              2B
    0xFF,   //              2C
    0xFF,   //              2D
    0xFF,   //              2E
    0xFF,   //              2F
    0x34,   // 0            30
    0x35,   // 1            31
    0x36,   // 2            32
    0x37,   // 3            33
    0x38,   // 4            34
    0x39,   // 5            35
    0x3A,   // 6            36
    0x3B,   // 7            37
    0x3C,   // 8            38
    0x3D,   // 9            39
    0xFF,   //              3A
    0xFF,   //              3B
    0xFF,   //              3C
    0xFF,   //              3D
    0xFF,   //              3E
    0xFF,   //              3F
    0xFF,   //              40
    0x1A,   // A            41
    0x1B,   // B            42
    0x1C,   // C            43
    0x1D,   // D            44
    0x1E,   // E            45
    0x1F,   // F            46
    0x20,   // G            47
    0x21,   // H            48
    0x22,   // I            49
    0x23,   // J            4A
    0x24,   // K            4B
    0x25,   // L            4C
    0x26,   // M            4D
    0x27,   // N            4E
    0x28,   // O            4F
    0x29,   // P            50
    0x2A,   // Q            51
    0x2B,   // R            52
    0x2C,   // S            53
    0x2D,   // T            54
    0x2E,   // U            55
    0x2F,   // V            56
    0x30,   // W            57
    0x31,   // X            58
    0x32,   // Y            59
    0x33,   // Z            5A
    0xFF,   //              5B
    0xFF,   //              5C
    0xFF,   //              5D
    0xFF,   //              5E
    0xFF,   //              5F
    0xFF,   //              60
    0x00,   // a            61
    0x01,   // b            62
    0x02,   // c            63
    0x03,   // d            64
    0x04,   // e            65
    0x05,   // f            66
    0x06,   // g            67
    0x07,   // h            68
    0x08,   // i            69
    0x09,   // j            6A
    0x0A,   // k            6B
    0x0B,   // l            6C
    0x0C,   // m            6D
    0x0D,   // n            6E
    0x0E,   // o            6F
    0x0F,   // p            70
    0x10,   // q            71
    0x11,   // r            72
    0x12,   // s            73
    0x13,   // t            74
    0x14,   // u            75
    0x15,   // v            76
    0x16,   // w            77
    0x17,   // x            78
    0x18,   // y            79
    0x19,   // z            7A
    0xFF,   //              7B
    0xFF,   //              7C
    0xFF,   //              7D
    0xFF,   //              7E
    0xFF    //              7F
};
