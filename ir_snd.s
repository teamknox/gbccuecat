;***********************************************************
;*                                                         *
;*              IR DRIVER for Color Game Boy               *
;*                 [ Ir-Sender  for RCX ]                  *
;*                                                         *
;*                        2000/02                          *
;*                   K.I. of TeamKNOx                      *
;*                                                         *
;*---------------------------------------------------------*
;* - Protocol : 2400bps/ 8bit/ Odd Parity/ Stopbit 1       *
;* - Carrier Frequency : 38KHz                             *
;* - Input : Send Data (1 byte)                            *
;***********************************************************
;4.194304MHz / 2400bps = 1747.6clock

	.area	_CODE
_ir_snd::
	di
	xor	a
	ldh	(#0x56),a	;read disable

;*** Send Data ***
	lda	hl,2(sp)	;skip return address
	ld	a,(hl)		;send data
	cpl
	ld	d,a

;*** Start Bit ***
snd_start:
	ld	a,#0x01
	call	trans_pulse38
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4

;*** Data Bit ***
	ld	e,#<0		;parity counter	; 8
	ld	c,#<8		;bit counter	; 8
snd_data:
	ld	a,d				; 4
	and	#0x01				; 4
	call	trans_pulse38			;24+1672
	ld	a,d				;	 4
	and	#0x01				;	 4
	add	a,e				;	 4
	ld	e,a				;	 4
	ld	a,d				;	 4
	rrca					;	 4
	ld	d,a				;	 4
	dec	c				;	 4
	jr	nz,snd_data			;	12(8)
	nop					;	 4

;*** Parity Bit ***
	ld	a,e				;	 4
	and	#0x01		;Odd Parity	;	 4
	call	trans_pulse38			;	24+1672
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4
	nop					; 4

;*** Stop Bit ***
snd_stop:
	xor	a				; 4
	call	trans_pulse38			;24+1672

	ei
	ret

;=================================================
; trans_pulse38 (Transmitting Pulse)
;	input		:A - Pulse signal(0 or 1)
;	destroyed	:B
;-------------------------------------------------
;	carrier frequency:38KHz
;		(4.194304MHz/38KHz=110.4clock)
;=================================================
trans_pulse38:
	ld	b,#<14		;	 8

1$:	ldh	(#0x56),a	;	12
	nop			; 4
	nop			; 4
	nop			; 4
	nop			; 4
	nop			; 4
	nop			; 4
	push	af		;16
	xor	a		; 4
	ldh	(#0x56),a	;12
	pop	af		;	12
	nop			;	 4
	nop			;	 4
	nop			;	 4
	nop			;	 4
	dec	b		;	 4
	jr	nz,1$		;	12(8)

	nop			;	 4
	ldh	(#0x56),a	;	12
	nop			;		 4
	nop			;		 4
	nop			;		 4
	nop			;		 4
	nop			;		 4
	nop			;		 4
	push	af		;		16
	xor	a		;		 4
	ldh	(#0x56),a	;		12
	pop	af		;			12
	nop			;			 4
	nop			;			 4
	nop			;			 4

	ret			;			16
		;------------------------------------------
		;total clock = 56*2*14+56+48 = 1672clock

;end of ir_snd.s
