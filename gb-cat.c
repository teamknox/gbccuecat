//
// GB :Cue:C.A.T. Reader
//     TeamKNOx
//     01/2001
//

#include <gb.h>
#include <stdlib.h>
#include "bkg.h"

// definition
#define OFF 0
#define ON 1

#define FOREVER 1
#define READING 1

#define CLEAR 0

#define CR 0x0d
#define LF 0x0a
#define SP 0x20

// character data
#include "bkg.c"
UWORD bkg_palette[] = {
	bkgCGBPal0c0, bkgCGBPal0c1, bkgCGBPal0c2, bkgCGBPal0c3,
};

// keyboad data
#include "109jp_kb_table.c"

// base64 encode data
#include "base64_table.c"

// message
unsigned char msg_title[]  = { ":Cue:C.A.T. Reader" };
unsigned char msg_credit[] = { "`2001 TeamKNOx" };
unsigned char msg_clr[]    = { "                    " };
unsigned char msg_spc[]    = { " " };
unsigned char msg_base64[] = { "base64" };
unsigned char msg_num[]    = { "serial no." };
unsigned char msg_type[]   = { "barcode type" };
unsigned char msg_data[]   = { "barcode data" };
unsigned char msg_frame1[] = { 16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,18,0 };
unsigned char msg_frame2[] = { 19,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,20,0 };
unsigned char msg_frame3[] = { 21,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,23,0 };
unsigned char msg_frame4[] = { 1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,0 };
unsigned char msg_frame5[] = { 4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0 };
unsigned char msg_frame6[] = { 5,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,6,0 };

// extern
extern UBYTE key_raw_ir[2];

// work area
UBYTE key_flag;
unsigned char raw[4][127];
unsigned char ascii[127];
UBYTE key_ascii_old;
UBYTE speed;
UBYTE parity;
UBYTE shift_mode;
UBYTE data1, data2, data3;
unsigned char serial_no[20];
unsigned char barcode_type[20];
unsigned char barcode_data[20];

// functoins
void init_character()
{
	set_bkg_data( 0, 128, bkg );
	set_bkg_palette( 0, 1, bkg_palette );
	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();
}

UBYTE getkey()
{
	UBYTE inkey;
	inkey = joypad();
	if( key_flag == OFF ) {
		if( inkey ) {
			key_flag = ON;
		}
	} else {
		if( !inkey ) {
			key_flag = OFF;
		}
		inkey = 0;
	}
	return( inkey );
}

void cls( UBYTE y1, UBYTE y2 )
{
	UBYTE y;
	for ( y=y1; y<y2+1; y++ ) {
		set_bkg_tiles( 0, y, 20, 1, msg_clr );
	}
}

void clr_mem()
{
	UBYTE i, j;
	for( i=0; i<4; i++ ) {
		for( j=0; j<127; j++ ) {
			raw[i][j] = CLEAR;
		}
	}
	for( i=0; i<127; i++ ) {
		ascii[i] = CLEAR;
	}
	for( i=0; i<20; i++ ) {
		serial_no[i]    = CLEAR;
		barcode_type[i] = CLEAR;
		barcode_data[i] = CLEAR;
	}
}

void disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
	UWORD m;
	UBYTE i, n;
	unsigned char data[6];

	m = 1;
	if ( c > 1 ) {
		for ( i = 1; i < c; i++ ) {
			m = m * e;
		}
	}
	for ( i = 0; i < c; i++ ) {
		n = d / m; d = d % m; m = m / e;
		data[i] = '0' + n; // '0' - '9'
		if ( data[i] > '9' )  data[i] += 7;
	}
	set_bkg_tiles( x, y, c, 1, data );
}

void irda_snd_dump()
{
	UBYTE i;

	i = 0;
	while( ascii[i] ) {
		irda_snd( ascii[i] );
		i++;
	}
	irda_snd( CR );
	irda_snd( LF );

	i = 0;
	while( serial_no[i] ) {
		irda_snd( serial_no[i] );
		i++;
	}
	irda_snd( CR );
	irda_snd( LF );

	i = 0;
	while( barcode_type[i] ) {
		irda_snd( barcode_type[i] );
		i++;
	}
	irda_snd( CR );
	irda_snd( LF );

	i = 0;
	while( barcode_data[i] ) {
		irda_snd( barcode_data[i] );
		i++;
	}
	irda_snd( CR );
	irda_snd( LF );
}

void ir_snd_dump()
{
	UBYTE i;

	i = 0;
	while( ascii[i] ) {
		ir_snd( ascii[i] );
		i++;
	}
	ir_snd( CR );
	ir_snd( LF );

	i = 0;
	while( serial_no[i] ) {
		ir_snd( serial_no[i] );
		i++;
	}
	ir_snd( CR );
	ir_snd( LF );

	i = 0;
	while( barcode_type[i] ) {
		ir_snd( barcode_type[i] );
		i++;
	}
	ir_snd( CR );
	ir_snd( LF );

	i = 0;
	while( barcode_data[i] ) {
		ir_snd( barcode_data[i] );
		i++;
	}
	ir_snd( CR );
	ir_snd( LF );
}

UBYTE convert_to_ascii( UBYTE key_raw )
{
	UBYTE key_ascii;

	if( key_raw & 0x80 ) { // check for key off
		key_ascii = 0xFF;
		return( key_ascii );
	}

	key_ascii = key_table[key_raw]; // convert to ascii code

	if( key_ascii == 0x0F ) { // check for shift key
		shift_mode ^= 0x01;
		key_ascii = 0xFF;
		return( key_ascii );
	}

	if( shift_mode == ON ) { // convert to shift char
		if( (key_ascii>0x60) && (key_ascii<0x7B) ) { // 'a' - 'z'
			key_ascii = key_ascii - 0x20;
		} else if( (key_ascii>0x30) && (key_ascii<0x3C) ) { // '1' - '9' ':' ';'
			key_ascii = key_ascii - 0x10;
		} else if( (key_ascii>0x2B) && (key_ascii<0x30) ) { // ',' '-' '.' '/'
			key_ascii = key_ascii + 0x10;
		} else if( (key_ascii>0x5A) && (key_ascii<0x5F) ) { //  '[' '\' ']' '^'
			key_ascii = 0x60;
		}else if( key_ascii == 0x40 ) { // '@'
			key_ascii = 0x60;
		} 
	}

	if( key_ascii_old == key_ascii ) { // check for key up
		key_ascii_old = 0xFF;
		key_ascii = 0xFF;
	} else {
		key_ascii_old = key_ascii;
	}

	return( key_ascii );
}

void encode( UBYTE asc1, UBYTE asc2, UBYTE asc3, UBYTE asc4 )
{
	UBYTE base1, base2, base3, base4;
	UBYTE d1, d2;
	// base64_encode
	base1 = base64_table[asc1];
	base2 = base64_table[asc2];
	base3 = base64_table[asc3];
	base4 = base64_table[asc4];
	// 6bit to 8bit convert
	d1 = ( base1 << 2 ) & 0xFC;
	d2 = ( base2 >> 4 ) & 0x03;
	data1 = ( d1 | d2 );
	d1 = ( base2 << 4 ) & 0xF0;
	d2 = ( base3 >> 2 ) & 0x0F;
	data2 = ( d1 | d2 );
	d1 = ( base3 << 6 ) & 0xC0;
	d2 = ( base4 ) & 0x3F;
	data3 = ( d1 | d2 );
	// decrypt by keycode'C'
	data1 ^= 'C';
	data2 ^= 'C';
	data3 ^= 'C';
}

void cat_cry()
{
  NR50_REG = 0x77;
  NR51_REG = 0x11;
  NR52_REG = 0x8F;

  NR10_REG = 0x4F;
  NR11_REG = 0x00;
  NR12_REG = 0xF4;
  NR13_REG = 0xFF;
  NR14_REG = 0x87;
}

void main()
{
	UBYTE i, j, k;
	UBYTE key, row, flag, mode, data;
	speed = 1; // 9600bps (for irda_send)
	parity = 0; // non (for irda_send)

	init_character();
	// initialize bank number
	ENABLE_RAM_MBC1;
	SWITCH_RAM_MBC1( 0 );
	cat_cry();

	while( FOREVER ) {
		cls( 0, 17 );
		// Display
		set_bkg_tiles( 0, 0, 20, 1, msg_frame4 );
		set_bkg_tiles( 0, 1, 20, 1, msg_frame5 );
		set_bkg_tiles( 1, 1, 18, 1, msg_title );
		set_bkg_tiles( 0, 2, 20, 1, msg_frame6 );
		set_bkg_tiles( 0, 3, 20, 1, msg_frame1 );
		set_bkg_tiles( 1, 3, 6, 1, msg_base64 );
		set_bkg_tiles( 0, 4, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 5, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 6, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 7, 20, 1, msg_frame3 );
		set_bkg_tiles( 0, 8, 20, 1, msg_frame1 );
		set_bkg_tiles( 1, 8, 10, 1, msg_num );
		set_bkg_tiles( 0, 9, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 10, 20, 1, msg_frame3 );
		set_bkg_tiles( 0, 11, 20, 1, msg_frame1 );
		set_bkg_tiles( 1, 11, 12, 1, msg_type );
		set_bkg_tiles( 0, 12, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 13, 20, 1, msg_frame3 );
		set_bkg_tiles( 0, 14, 20, 1, msg_frame1 );
		set_bkg_tiles( 1, 14, 12, 1, msg_data );
		set_bkg_tiles( 0, 15, 20, 1, msg_frame2 );
		set_bkg_tiles( 0, 16, 20, 1, msg_frame3 );
		set_bkg_tiles( 3, 17, 14, 1, msg_credit );

		clr_mem();
		// Wait BarCode signal
		while( !(raw[0][0]) ) {
			flag = READING;
			i = j = 0;
			while( flag && (i<4) ) {
				flag = get_raw_signal_key();
				raw[i][j] = key_raw_ir[0];
				j++;
				if( j == 127 ) {
					i++;
					j = 0;
				}
			}
		}

		// Convert to Ascii
		key_ascii_old = 0xFF;
		shift_mode = OFF;
		k = 0;
		for ( i=0; i<4; i++ ) {
			for ( j=0; j<127; j++ ) {
				if ( raw[i][j] ){
					data = convert_to_ascii( raw[i][j] );
					if( (data > 0x1F) && (data < 0x80) ) { // ascii code 0x20 to 0x7F
						ascii[k] = data;
						k++;
					}
				}
			}
		}

		set_bkg_tiles( 1, 4, 18, 3, ascii );

		k = 0;
		while( ascii[k] != '.' ) {
			k++;
		}
		k++;

		// Get the serial number
		i = 0;
		while( ascii[k] != '.' ) {
			encode( ascii[k], ascii[k+1], ascii[k+2], ascii[k+3] );
			serial_no[i]   = data1;
			serial_no[i+1] = data2;
			serial_no[i+2] = data3;
			k = k + 4;
			i = i + 3;
		}
		k++;
		// Get the barcode type
		i = 0;
		while( ascii[k] != '.' ) {
			encode( ascii[k], ascii[k+1], ascii[k+2], ascii[k+3] );
			barcode_type[i]   = data1;
			barcode_type[i+1] = data2;
			barcode_type[i+2] = data3;
			k = k + 4;
			i = i + 3;
		}
		k++;
		// Get the barcode data
		i = 0;
		while( (ascii[k]!='.') && (ascii[k]) ) {
			encode( ascii[k], ascii[k+1], ascii[k+2], ascii[k+3] );
			if( ascii[k+2] == '.' ) data2 = data3 = CLEAR;
			if( ascii[k+3] == '.' ) data3 = CLEAR;
			barcode_data[i]   = data1;
			barcode_data[i+1] = data2;
			barcode_data[i+2] = data3;
			k = k + 4;
			i = i + 3;
		}

		cat_cry();
		set_bkg_tiles( 1, 9, 18, 1, serial_no );
		set_bkg_tiles( 1, 12, 18, 1, barcode_type );
		set_bkg_tiles( 1, 15, 18, 1, barcode_data );

		key = CLEAR;
		while(key != J_START) {
			key = getkey();
			switch( key ) {
				case J_A:
					irda_snd_dump();
					break;
				case J_B:
					ir_snd_dump();
					break;
			}
		}
	}
}

// end of gb_cat.c
